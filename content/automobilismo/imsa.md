---
title: "Imsa"
date: 2021-09-18T23:28:40-03:00
draft: 
---

A IMSA, sigla para International Motor Sports Association, é uma categoria de corridas de automobilismo de resistência e esporte a motor nos Estados Unidos. A IMSA é conhecida por suas séries de corridas de longa duração, como o Campeonato WeatherTech SportsCar e o Campeonato Michelin Pilot Challenge. Alguns pontos-chave sobre a IMSA incluem:

&nbsp;

1. Corridas de resistência: A IMSA é conhecida por suas corridas de resistência, incluindo eventos icônicos como as 24 Horas de Daytona, as 12 Horas de Sebring e as 6 Horas de Watkins Glen. Essas corridas testam a confiabilidade dos carros e a resistência dos pilotos.Inclusive um dos atuais campeoes da 24 Horas de Daytona é o brasileiro Helio Castroneves.

&nbsp;

&nbsp;

&nbsp;

![](https://cdn-2.motorsport.com/images/amp/0oOEVPj0/s1000/imsa-daytona-24-hours-2023-60--2.jpg)

&nbsp;

&nbsp;

&nbsp;

2. Classes de carros: A IMSA apresenta uma variedade de classes de carros, cada uma com suas próprias especificações técnicas. Isso inclui carros esportivos, protótipos e GTs, tornando as corridas emocionantes e competitivas.

&nbsp;

&nbsp;

&nbsp;

![](https://gt-place.com/wp-content/uploads/2022/01/GALSTAD-R24-0122-17167-1024x683.jpg)

&nbsp;

&nbsp;

&nbsp;

3. Pilotos e equipes: A IMSA atrai pilotos e equipes de todo o mundo, incluindo muitos pilotos de renome internacional. As equipes competem em carros de várias marcas, como Porsche, Ferrari, Chevrolet e muitas outras.

&nbsp;

&nbsp;

&nbsp;

![](https://www.imsa.com/wp-content/uploads/sites/32/2022/01/18/LAT-Laguna-091221-PB-02006_2022-01-18.jpg)

&nbsp;

&nbsp;

&nbsp;

4. Calendário de corridas: A IMSA realiza corridas em vários circuitos nos Estados Unidos e Canadá, oferecendo uma programação diversificada que atrai um público amplo.

&nbsp;

&nbsp;

&nbsp;

![](https://www.imsa.com/wp-content/uploads/sites/32/2023/08/04/2024-IWSC-Schedule_classes-1024x576.png)

&nbsp;

&nbsp;

&nbsp;

A IMSA é altamente respeitada no mundo do automobilismo e oferece corridas emocionantes e desafiadoras que atraem fãs e competidores apaixonados por carros de resistência e esporte a motor.